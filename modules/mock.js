/** 模拟模块 */

/**
 * @name: getRndColor
 * @cname: 生成随机色
 * @desc: 随机生成16位的颜色
 * @param 无参数
 * @panme: _.getRndColor()
 * @result: #FB3E6C
*/
export function getRndColor() {
    let colorStr = "#";
    let str = "ABCDEF123456789";
    for (let i = 0; i < 6; i++) {
        colorStr += str[Math.floor(Math.random() * str.length)];
    }
    return colorStr;
}

/**
 * @name: getProvinceEName
 * @cname: 汉字转拼音
 * @desc: 用于省份汉字与拼音转换，有时候地图中需要用
 * @param  {*} province 
 * @panme: province=北京
 * @result: beijing
*/
export function getProvinceEName(province) {
    let obj = { "北京": "beijing", "上海": "shanghai", "天津": "tianjin", "重庆": "chongqing", "香港": "xianggang", "澳门": "aomen", "安徽": "anhui", "福建": "fujian", "广东": "guangdong", "广西": "guangxi", "贵州": "guizhou", "甘肃": "gansu", "海南": "hainan", "河北": "hebei", "河南": "henan", "黑龙江": "heilongjiang", "湖北": "hubei", "湖南": "hunan", "吉林": "jilin", "江苏": "jiangsu", "江西": "jiangxi", "辽宁": "liaoning", "内蒙古": "neimenggu", "宁夏": "ningxia", "青海": "qinghai", "陕西": "shanxi1", "山西": "shanxi", "山东": "shandong", "四川": "sichuan", "台湾": "taiwan", "西藏": "xizang", "新疆": "xinjiang", "云南": "yunnan", "浙江": "zhejiang" }
    return obj[province]
}